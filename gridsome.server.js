const axios = require("axios");
// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = function(api) {
  api.loadSource(async ({ addContentType }) => {
    const { data } = await axios.get(
      "https://deliver.kontent.ai/035f23e2-e10a-007e-18df-f3107ec6edad/items"
    );

    const InfoModuleContentType = addContentType({
      typeName: "InfoModule",
      route: "/info/",
    });

    for (const item of data.items) {
      InfoModuleContentType.addNode({
        title: item.elements.title.value,
      });
      console.log("InfoModuleContentType", item);
    }
  });

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api
  });
};
