const path = require("path");
const tailwind = require("tailwindcss");
const purgecss = require("@fullhuman/postcss-purgecss");

function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/assets/sass/main.scss")],
    });
}

const postcssPlugins = [tailwind()];

if (process.env.NODE_ENV === "production")
  postcssPlugins.push(purgecss(require("./purgecss.config.js")));

module.exports = {
  chainWebpack(config) {
    // Load variables for all vue-files
    const types = ["vue-modules", "vue", "normal-modules", "normal"];

    types.forEach((type) => {
      addStyleResource(config.module.rule("scss").oneOf(type));
    });
  },
  siteName: "Gridsome",
  plugins: [
    {
      use: "@gridsome/source-filesystem",
      options: {
        path: "src/pages/markdown/**/*.md",
        typeName: "Post",
        remark: {
          // remark options
        },
      },
    },
    {
      use: "@gridsome/source-wordpress",
      options: {
        baseUrl: "http://localhost:8888/wordpress/index.php", // required
        apiBase: "wp-json",
        typeName: "WordPress",
        perPage: 100,
        concurrent: 10,
        routes: {
          post: "/:year/:month/:day/:slug",
          post_tag: "/tag/:slug",
        },
        customEndpoints: [
          {
            typeName: "Acf",
            route:
              "http://localhost:8888/wordpress/index.php/wp-json/acf/v3/pages",
          },
        ],
      },
    },
  ],
  css: {
    loaderOptions: {
      postcss: {
        plugins: postcssPlugins,
      },
    },
  },
};
